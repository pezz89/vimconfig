" Vim settings
let g:python_host_prog='/usr/bin/python'

filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
set rtp+=/usr/local/Cellar/fzf/HEAD
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'
Plugin 'Valloric/YouCompleteMe'
Plugin 'bling/vim-airline'
Plugin 'docunext/closetag.vim'
Plugin 'rizzatti/dash.vim'
Plugin 'terryma/vim-expand-region'
Plugin 'tpope/vim-fugitive'
Plugin 'othree/html5.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'klen/python-mode'
Plugin 'altercation/vim-colors-solarized'
Plugin 'rstacruz/sparkup'
Plugin 'scrooloose/syntastic'
Plugin 'majutsushi/tagbar'
Plugin 'christoomey/vim-tmux-navigator'
Plugin 'jelera/vim-javascript-syntax'
Plugin 'tpope/vim-obsession'
Plugin 'nvie/vim-togglemouse'
Plugin 'benmills/vimux'
Plugin 'julienr/vimux-pyutils'
Plugin 'embear/vim-localvimrc'
Plugin 'tpope/vim-surround'
Plugin 'Lokaltog/vim-easymotion'
Plugin 'Yggdroot/indentLine'
Plugin 'mindriot101/vim-yapf'
call vundle#end()            " required
filetype on
filetype plugin on
filetype indent on
" ====================================
" General vim settings
" ====================================
" Set the default shell
set shell=zsh\ -l

" Set spell checker language
set spelllang=en_gb

" Allow the use of the mouse in vim
set mouse=a

"Disable .swp files
set noswapfile

" set the ruler to display cursor position on
set ruler

" set syntax highlighting on
syntax on

" set tabs to have 4 spaces
set ts=4

" indent when moving to the next line while writing code
set autoindent

" expand tabs into spaces
set expandtab

" when using the >> or << commands, shift lines by 4 spaces
set shiftwidth=4

" remove 4 spaces as if it were a tab
set softtabstop=4

" show a visual line under the cursor's current line 
set cursorline

" enable all Python syntax highlighting features
let python_highlight_all = 1

" show the matching part of the pair for [] {} and ()
set showmatch

" 
set nu
            
" Set numbers to display relative to current cursor position in the file
set relativenumber

" Ignore case when using search
set ignorecase

" Show search result before moving to it
set incsearch

" ====================================
" Indent highlighting settings
" ====================================
let g:indentLine_char = '▸'
let g:indentLine_color_term = 0
let g:indentLine_enabled = 0

" ====================================
" solarized settings
" ====================================
" Set to the dark solarized colour scheme
set background=dark
let g:solarized_termcolors=16
let g:solarized_visibility="high"
set t_Co=256                        " force vim to use 256 colors
colorscheme solarized

" ====================================
" NerdTree Settings
" ====================================
"Fix nerdtree arrow problems
let g:NERDTreeDirArrows=0

" ====================================
" LocalVimrc Settings
" ====================================
" Set events to check for local vimrcs on
let g:localvimrc_event = [ "BufWinEnter", "VimEnter" ]
" Save preferences for which lvimrc files to load so vim doesn't ask every
" time
let g:localvimrc_persistent = 2

" ====================================
" Airline settings 
" ====================================
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#fnamemod = ':t'
let g:airline#extensions#tabline#buffer_nr_format = '%s: '
"Remove seperators and makes airline look cleaner
let g:airline_left_sep=''
let g:airline_right_sep=''
let g:airline_section_z=''
" Show airline even when only one file is open
set laststatus=2
" This allows buffers to be hidden if you've modified a buffer.
" This is almost a must if you wish to use buffers in this way.
set hidden
let g:airline#extensions#tabline#buffer_idx_mode = 1
nmap <leader>1 <Plug>AirlineSelectTab1
nmap <leader>2 <Plug>AirlineSelectTab2
nmap <leader>3 <Plug>AirlineSelectTab3
nmap <leader>4 <Plug>AirlineSelectTab4
nmap <leader>5 <Plug>AirlineSelectTab5
nmap <leader>6 <Plug>AirlineSelectTab6
nmap <leader>7 <Plug>AirlineSelectTab7
nmap <leader>8 <Plug>AirlineSelectTab8
nmap <leader>9 <Plug>AirlineSelectTab9

" ====================================
" Closetag for HTML settings...
" ====================================

autocmd FileType html,htmldjango,jinjahtml,eruby,mako let b:closetag_html_style=1
autocmd FileType html,xhtml,xml,htmldjango,jinjahtml,eruby,mako source ~/.vim/bundle/closetag.vim/plugin/closetag.vim
autocmd FileType html,xhtml,xml,htmldjango,jinjahtml,eruby,mako set smarttab

" ====================================
" Pymode setting
" ====================================

let g:pymode_options = 1

" Disable automatic folding of classes and functions
let g:pymode_folding = 0

" Set the maximum line length before wrapping text
let g:pymode_options_max_line_length = 79

" Set the display of a coloured column to represent the maximum line length
" location
let g:pymode_options_colorcolumn = 1

" Turn indenting on
let g:pymode_indent = 1

" Don't check for errors every time a file is written
let g:pymode_lint_on_write = 0

" Turn linting off
let g:pymode_lint = 0

" Turn syntax highlighting on
let g:pymode_syntax = 1

" Disable rope due to hanging bug
let g:pymode_rope = 0

" Enable all extra syntax highlighting options
let g:pymode_syntax_all = 1

let g:pymode_run = 0
let g:pymode_run_bind = ''

" ====================================
" ProSession Settings
" ====================================
let g:prosession_tmux_title = 1
let g:prosession_load_on_startup = 1

" ====================================
" Syntastic
" ====================================
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1

" Check for errors when opening a new file
let g:syntastic_check_on_open = 1

" Don't check for errors if quiting after writing
let g:syntastic_check_on_wq = 0

" Set the checkers to be used for finding syntax errors
let g:syntastic_python_checkers = ['pep8', 'pyflakes', 'pep257']

function! TogglePep257()
    let c = 0
    let found = 0
    for i in g:syntastic_python_checkers
        if i == 'pep257'
            call remove(g:syntastic_python_checkers, c)
            let found = 1
        endif
        let c += 1
    endfor
    if found == 0
        call add(g:syntastic_python_checkers, 'pep257')
    endif
    :SyntasticCheck
endfunction

" Function for toggling the error window
function! ToggleErrors()
    let old_last_winnr = winnr('$')
    lclose
    if old_last_winnr == winnr('$')
        " Nothing was closed, open syntastic error location panel
        Errors
    endif
endfunction

let g:syntastic_auto_loc_list = 0

" Run all checkers and then create list of errors rather than showing error
" lists individually
let g:syntastic_aggregate_errors = 1

" Set JavaScript syntax checkers
let g:syntastic_javascript_checkers = ['jshint']

autocmd CursorMovedI * if pumvisible() == 0|pclose|endif
autocmd InsertLeave * if pumvisible() == 0|pclose|endif

set conceallevel=2
set concealcursor=vin

" Complete options (disable preview scratch window, longest removed to aways
" show menu)
set completeopt=preview,preview

" Limit popup menu height
set pumheight=20"

" ====================================
" YouCompleteMe Settings
" ====================================
let g:ycm_path_to_python_interpreter ='/usr/bin/python'

" ====================================
" VIMUX Commands
" ====================================
" Prompt for a command to run
map <Leader>vp :VimuxPromptCommand<CR>

" Run last command executed by RunVimTmuxCommand
map <Leader>vl :VimuxRunLastCommand<CR>

" Inspect runner pane
map <Leader>vi :VimuxInspectRunner<CR>

" Close all other tmux panes in current window
map <Leader>vq :VimuxCloseRunner<CR>

" Interrupt any command running in the runner pane
map <Leader>vs :VimuxInterruptRunner<CR>

" ====================================
" Tagbar Settings
" ====================================
" Focus on tagbar when it is opened
let g:tagbar_autofocus = 1

" ====================================
" Custom key bindings
" ====================================
" Disable arrow keys
map <Up>    <nop>
map <Down>  <nop>
map <Left>  <nop>
map <Right> <nop>

" Set space as leader key
map <Space> <Leader>

" Backspace over tabs properly (I think...)
set backspace=indent,eol,start

" Show commands used in the botom right
set showcmd

" Activate/deactivate the Tagbar 
nmap <F8> :TagbarToggle<CR>

" Map the error window toggle to <F6>
nmap <F6> :call ToggleErrors()<CR><CR>

nmap <F5> :call TogglePep257()<CR>

" To open a new empty buffer
" This replaces :tabnew which I used to bind to this mapping
nmap <leader>t :enew<CR>

" Move to the next buffer
nmap <leader>l :bnext<CR>

" Move to the previous buffer
nmap <leader>h :bprevious<CR>

" Close the current buffer and move to the previous one
" This replicates the idea of closing a tab
nmap <leader>bq :bp <BAR> bd! #<CR>

" Show all open buffers and their status
nmap <leader>bl :ls<CR>

" Toggle the NerdTree file browser menu
nmap <F7> :NERDTreeToggle<CR>

" Toggle session monitoring
nmap <leader>o :Obsess<CR>

" Copy too and from the system clipboard with standard yank and paste commands
" but with leader
vmap <Leader>y "+y
vmap <Leader>d "+d
nmap <Leader>p "+p
nmap <Leader>P "+P
vmap <Leader>p "+p
vmap <Leader>P "+P

" Set clipboard to system clipboard
set clipboard=unnamed

" Enter visual line by hitting leader twice
nmap <Leader><Leader> V

" Map v to use expand-region plugin for selecting char, then word, then
" paragraph 
vmap v <Plug>(expand_region_expand)
vmap <C-v> <Plug>(expand_region_shrink)

" Stop strange window from popping up when accidentally press q:
map q: :q

" Navigate vim windows with hjkl
nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k
nnoremap <c-h> <c-w>h
nnoremap <c-l> <c-w>l

" Quickly edit/reload the vimrc file
nmap <silent> <leader>ev :e $MYVIMRC<CR>
nmap <silent> <leader>sv :so $MYVIMRC<CR>

" Call yapf formatting
nnoremap <leader>y :call Yapf()<cr>
" ====================================

" Source local vim script for the current machine
" (Keep this at bottom to overwrite configs above with system specific
" configs)
"if filereadable(expand("./vimrc.local"))
"    source ./vimrc.local
"endif

